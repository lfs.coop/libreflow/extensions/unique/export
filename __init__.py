import os
import re
import glob
import subprocess
from kabaret import flow
from kabaret.flow_contextual_dict import get_contextual_dict


class RevisionNameChoiceValue(flow.values.SessionValue):
    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _action = flow.Parent()
    _file = flow.Parent(2)

    def __init__(self, parent, name):
        super(RevisionNameChoiceValue, self).__init__(parent, name)
        self._revision_names = None

    def choices(self):
        if self._revision_names is None:
            self._revision_names = self._file.get_revision_names(sync_status='Available', published_only=True)
        
        return self._revision_names
    
    def revert_to_default(self):
        watched = self._watched
        self.set_watched(False)
        names = self.choices()
        if names:
            self.set(names[-1])
        self.set_watched(watched)
    
    def touch(self):
        self._revision_names = None
        super(RevisionNameChoiceValue, self).touch()
    
    def refresh(self):
        self.touch()
        self.revert_to_default()
    
    def _fill_ui(self, ui):
        ui['hidden'] = not self._action.ffmpeg_configured()


class CreateFinalMovie(flow.Action):
    revision = flow.SessionParam(value_type=RevisionNameChoiceValue).watched()
    _renders_found = flow.Computed(cached=True)
    _render_folder = flow.Parent()
    _files = flow.Parent(2)
    _task = flow.Parent(3)

    def needs_dialog(self):
        self.revision.refresh()
        self._renders_found.touch()
        if not self.ffmpeg_configured():
            self.message.set('You need to set the path to ffmpeg '
                'in the project settings.')
        elif not self._renders_found.get():
            self.message.set('<h2><font color=#D5000D>Renders not '
                'found.</font></h2>')
        else:
            self.message.set('<h2>Create final movie ?</h2>'
                'It will be available in <tt>compositing_movie_final.mov '
                f'({self.revision.get()})</tt>.')
        
        return True
    
    def get_buttons(self):
        if not self.ffmpeg_configured():
            return ['Cancel']
        
        return ['Confirm', 'Cancel']
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        if not self._renders_found.get():
            return self.get_result(close=False)
        
        src_rev = self._render_folder.get_revision(self.revision.get())
        settings = get_contextual_dict(self, 'settings')
        seq_format = f"{settings['sequence']}_{settings['shot']}.%04d.png"

        ffmpeg_bin = self.root().project().admin.project_settings.ffmpeg_path.get()
        ffmpeg_args = [ffmpeg_bin, '-y', '-loglevel', 'error']
        ffmpeg_args.extend(['-start_number', str(self.get_start_frame(src_rev))])
        ffmpeg_args.extend(['-i', os.path.join(src_rev.get_path(), seq_format)])
        ffmpeg_args.extend(['-c:v', 'dnxhd'])
        ffmpeg_args.extend(['-vf', 'scale=3840:2160,fps=25,format=yuv422p10le'])
        ffmpeg_args.extend(['-profile:v', 'dnxhr_hqx'])
        ffmpeg_args.extend(['-b:v', '729M'])
        ffmpeg_args.append(self.ensure_output_revision(src_rev.name()))
        
        print('ffmpeg command: '+' '.join(ffmpeg_args) + '\n')
        
        subprocess.run(ffmpeg_args)
    
    def get_start_frame(self, revision):
        def natural_sort_keys(text):
            return [
                int(c) if c.isdigit() else c
                for c in re.split(r'(\d+)', text)
            ]
        
        img_paths = glob.glob(revision.get_path() + '/*.png')
        img_paths.sort(key=natural_sort_keys)
        m = re.match(r'.*\.(\d+)\.png', img_paths[0])
        start_num = int(m.group(1)) if m and m.group(1) else -1
        
        return start_num
    
    def ensure_output_revision(self, revision_name):
        if self._files.has_file('compositing_movie_final', 'mov'):
            mov = self._files['compositing_movie_final_mov']
        else:
            tm = self.root().project().get_task_manager()
            path_format = None
            default_files = tm.get_task_files(self._task.name())
            if 'compositing_movie_final_mov' in default_files:
                path_format = default_files['compositing_movie_final_mov'][1]
            else:
                path_format = tm.get_task_path_format(self._task.name())
            
            mov = self._files.add_file('compositing_movie_final', 'mov', tracked=True, default_path_format=path_format)
            mov.file_type.set('Outputs')
        
        if not mov.has_revision(revision_name):
            rev = mov.add_revision(revision_name)
        else:
            rev = mov.get_revision(revision_name)
        os.makedirs(os.path.dirname(rev.get_path()), exist_ok=True)

        return rev.get_path()
    
    def ffmpeg_configured(self):
        ffmpeg_path = self.root().project().admin.project_settings.ffmpeg_path.get()
        return ffmpeg_path and os.path.exists(ffmpeg_path)
    
    def child_value_changed(self, child_value):
        if child_value is self.revision:
            self._renders_found.touch()
            if not self._renders_found.get():
                self.message.set('<h2><font color=#D5000D>Renders '
                    'not found.</font></h2>')
            else:
                self.message.set('<h2>Create final movie ?</h2>'
                    'It will be available in <tt>compositing_movie_final.mov '
                    f'({self.revision.get()})</tt>.')
    
    def compute_child_value(self, child_value):
        if child_value is self._renders_found:
            rev = self._render_folder.get_revision(self.revision.get())
            self._renders_found.set(
                rev and os.path.exists(rev.get_path())
                and glob.glob(rev.get_path() + '/*.png')
            )


def create_final_movie_action(parent):
    if parent.name() == 'compositing_render':
        r = flow.Child(CreateFinalMovie)
        r.name = 'create_final_movie'
        return r


def install_extensions(session):
    return {
        'unique': [
            create_final_movie_action,
        ]
    }
